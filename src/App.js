import React, { Component } from 'react'
import './App.css'
import Home from './home/Home.js'
import NotFound from './NotFound.js'
import About from './about/About.js'
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'
import {NavItem, Nav, MenuItem, NavDropdown, Navbar} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'

class App extends Component {
  render () {
    return (
      <Router>
        <div>
          <Navbar inverse collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <Link to='/'>Home</Link>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <LinkContainer to='/about'>
                  <NavItem>About</NavItem>
                </LinkContainer>
                <NavItem eventKey={2} href='#'>Link</NavItem>
                <NavDropdown eventKey={3} title='Dropdown' id='basic-nav-dropdown'>
                  <MenuItem eventKey={3.1}>Action</MenuItem>
                  <MenuItem eventKey={3.2}>Another action</MenuItem>
                  <MenuItem eventKey={3.3}>Something else here</MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey={3.3}>Separated link</MenuItem>
                </NavDropdown>
              </Nav>
              <Nav pullRight>
                <NavItem eventKey={1} href='#'>Link Right</NavItem>
                <NavItem eventKey={2} href='#'>Link Right</NavItem>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/about' component={About} />
            <Route render={({history: {location}}) => <NotFound location={location.pathname} text='Not Found'/>} />
          </Switch>
        </div>
      </Router>
    )
  }
  }

export default App
