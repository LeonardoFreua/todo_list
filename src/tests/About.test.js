import React from 'react';
import { expect } from 'chai';
import { render, mount   } from 'enzyme';

import About from '../about/About';

describe('<About />', () => {
    it('renders the text about page', () => {
        const wrapper = render(<About />);
        expect(wrapper.text()).to.contain('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed est id mi viverra laoreet a a risus. Nunc eget hendrerit nisl.');
    });

    it('renders the correct text if user is admin', () => {
        const wrapper = render (<About admin={true}/>);
        expect(wrapper.text()).to.contain('The user is admin');
    });

    it('renders the correct text if user is not admin', () => {
        const wrapper = render (<About admin={false}/>);
        expect(wrapper.text()).to.contain('The user is not admin');
    });

    //Check tag
    it('renders three tags <p>', () => {
        const wrapper = render (<About />);
        expect(wrapper.find('p')).to.have.length(3);
    });

    //Mount test props
    it('has correct props', () => {
        const wrapper = mount (<About admin={true}/>);
        expect(wrapper.props().admin).to.equal(true);
    });
});