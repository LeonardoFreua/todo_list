import React from 'react';
import { expect } from 'chai';
import { render } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom'

import NotFound from '../NotFound';

describe('<NotFound />', () => {
    it('renders the text', () => {
        const wrapper = render(<Router><NotFound text='' location='page' /></Router>);
        expect(wrapper.text()).to.contain('Not found!');
    });
});
