import React from 'react';
import { expect } from 'chai';
import { render, mount, findWithTypeAndText } from 'enzyme';
import sinon from 'sinon';

import ModalView from '../ModalView';

describe('<ModalView />', () => {
    it('has correct state', () => {
        const wrapper = mount(<ModalView />);
        expect(wrapper.state().showModal).to.equal(false);
    });

    it('changes state after button click', () => {
        const wrapper = mount(<ModalView/>);
        expect(wrapper.state().showModal).to.equal(false);
        wrapper.findWhere(n => n.type() === 'button' && n.contains('Open modal')).simulate('click');
        expect(wrapper.state().showModal).to.equal(true);
    });
});