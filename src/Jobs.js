import React, { Component } from 'react'
import $ from 'jquery'
import {ListGroupItem, ListGroup} from 'react-bootstrap'

class Jobs extends Component {
  constructor (props) {
    super(props)
    this.state = {jobs: []}
  }

  componentDidMount () {
    const self = this
    $.ajax({
      url: 'https://jobs.github.com/positions.json?description=python&location=san+francisco',

       // The name of the callback parameter, as specified by the YQL service
      jsonp: 'callback',

       // Tell jQuery we're expecting JSONP
      dataType: 'jsonp',

       // Work with the response
      success: function (response) {
        self.setState({
          jobs: response
        })
      }
    })
  }

  render () {
    return (
      <ListGroup>
        {
            this.state.jobs.map(function (job, index) {
              return (<ListGroupItem key={index} >{job.title}</ListGroupItem>)
            }, this)
          }
      </ListGroup>
    )
  }
  }

export default Jobs
