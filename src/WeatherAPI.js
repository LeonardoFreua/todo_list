import React, { Component } from 'react'
import {Panel, FormGroup, FormControl} from 'react-bootstrap'
import $ from 'jquery'

class WeatherAPI extends Component {
  constructor (props) {
    super(props)
    this.state = {weather: 0, cityId: 3452925, cityName: ''}
    this.changeCity = this.changeCity.bind(this)
    this.callApi = this.callApi.bind(this)
  }

  componentDidMount () {
    this.callApi()
  }

  changeCity (event) {
    this.setState({
      cityId: event.target.value
    })
    this.callApi(event.target.value)
  }

  callApi (id) {
    const self = this
    $.ajax({
      url: `https://api.openweathermap.org/data/2.5/weather?id=${id || this.state.cityId}&appid=fa9fc61221368149ed78be33c218d560`,

         // The name of the callback parameter, as specified by the YQL service
      jsonp: 'callback',

         // Tell jQuery we're expecting JSONP
      dataType: 'jsonp',

         // Work with the response
      success: function (response) {
        self.setState({
          weather: Math.round(response.main.temp - 273),
          cityName: response.name
        })
      }
    })
  }

  render () {
    return (
      <Panel header='Weather' bsStyle='info'>
        <FormGroup controlId='formControlsSelectMultiple'>
          <FormControl value={this.state.cityId} onChange={this.changeCity} componentClass='select'>
            <option value='3452925'>Porto Alegre</option>
            <option value='3467467'>Canoas</option>
            <option value='6173331'>Vancouver</option>
          </FormControl>
        </FormGroup>
        {this.state.weather} °C
            </Panel>
    )
  }
}

export default WeatherAPI
