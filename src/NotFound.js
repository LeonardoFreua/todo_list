import React from 'react'
import {Link} from 'react-router-dom'

const NotFound = ({text, location}) => {  
  return (
    <div>
      <p>Not found!</p>
      <h1>{text}</h1><br />
      <h1>{location}</h1><br />
      <Link to='/'>Home</Link>
    </div>
  )
}
export default NotFound
