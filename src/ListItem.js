import React, { Component } from 'react'
import {Button, Row} from 'react-bootstrap'

class ListItem extends Component {
  constructor (props) {
    super(props)
    this.deleteTask = this.deleteTask.bind(this)
  }

  deleteTask () {
    this.props.removeTask(this.props.index)
  }

  render () {
    const {task} = this.props
    return (
      <Row>
        <li>{task}<Button bsStyle='danger' onClick={this.deleteTask}>Remove</Button></li>
      </Row>
    )
  }
  }

export default ListItem
