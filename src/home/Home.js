import React, { Component } from 'react'
import logo from '../logo.svg'
import {FormGroup, FormControl, Button, Alert, Row, Col} from 'react-bootstrap'
import ListItem from '../ListItem'
import Jobs from '../Jobs'
import Product from '../Product'
import WeatherAPI from '../WeatherAPI'
import ModalView from '../ModalView'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {tasks: [], value: '', showAlert: false}
    this.handleChange = this.handleChange.bind(this)
    this.addTask = this.addTask.bind(this)
    this.removeTask = this.removeTask.bind(this)
  }

  handleChange (event) {
    this.setState({
      value: event.target.value
    })
  }

  addTask () {
    if (this.state.value.trim() !== '') {
      var temp = this.state.tasks
      temp.push(this.state.value)
      this.setState({
        tasks: temp, value: '', showAlert: false
      })
    } else {
      this.setState({
        showAlert: true
      })
    }
  }

  removeTask (index) {
    var temp = this.state.tasks
    temp.splice(index, 1)
    this.setState({
      tasks: temp
    })
  }

  render () {
    const alert = this.state.showAlert ? <Alert bsStyle='danger'><strong>O campo não pode ser vazio</strong></Alert> : null
    return (
      <div className='App'>
        <p className='App-intro'>
              To get started, edit <code>src/App.js</code> and save to reload.
            </p>
        <div className='container'>
          {/* <Product /> */}
          <Row className='show-grid'>
            <Col md={6} mdPush={6}><WeatherAPI /></Col>
            <Col md={6} mdPull={6}><Jobs /></Col>
          </Row>
          <Row className='show-grid'>
            <ModalView modalTitle='Modal title' buttonName='OK' btnStyle='success' showCancelButton>
              <h1>Descrição</h1>
              <h1>Descrição</h1>
              <h1>Descrição</h1>
              <h1>Descrição</h1>
            </ModalView>
          </Row><br />
          <form>
            <FormGroup controlId='formBasicText' >
              <FormControl
                type='text'
                value={this.state.value}
                placeholder='Enter text'
                onChange={this.handleChange} />
            </FormGroup>
            {alert}
            <Button bsStyle='primary' onClick={this.addTask}>Save</Button>
            <ul>
              {
                    this.state.tasks.map(function (task, index) {
                      return (<ListItem key={index} index={index} task={task} removeTask={this.removeTask} />)
                    }, this)
                  }
            </ul>
          </form>
          <ShowText post='texto do post' />
          <ShowText2 post='texto do post2' />
        </div>
      </div>
    )
  }
}

class ShowText extends Component {
  render () {
    return (
      <p>Example text: {this.props.post}</p>
    )
  }
}

const ShowText2 = ({post}) => {
  return (
    <p>Example text2: {post}</p>
  )
}

export default Home
