import React, { Component } from 'react'
import {Button, Modal} from 'react-bootstrap'

class ModalView extends Component {
  constructor (props) {
    super(props)
    this.state = {showModal: false}
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  openModal () {
    this.setState({
      showModal: true
    })
  }

  closeModal () {
    this.setState({
      showModal: false
    })
  }

  render () {
    const cancelButton = this.props.showCancelButton ? <Button onClick={this.closeModal} bsStyle='danger'>Cancel</Button> : null
    return (
      <div>
        <Button bsStyle='info' onClick={this.openModal}>Open modal</Button>

        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.props.children}
          </Modal.Body>
          <Modal.Footer>
            {cancelButton}
            <Button onClick={this.closeModal} bsStyle={this.props.btnStyle}>{this.props.buttonName}</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

export default ModalView
