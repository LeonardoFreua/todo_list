import React, { Component } from 'react'

const About = ({admin}) => {
  const checkAdmin = admin ? <p>The user is admin</p> : <p>The user is not admin</p> 
  return (
    <div className='App'>
      {checkAdmin}
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed est id mi viverra laoreet a a risus. Nunc eget hendrerit nisl.</p>
    </div>
  )
}

export default About
