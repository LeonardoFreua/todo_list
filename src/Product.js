import React, { Component } from 'react'
import $ from 'jquery'
import {Button, ListGroupItem, ListGroup} from 'react-bootstrap'

class Product extends Component {
  constructor (props) {
    super(props)
    this.state = {products: []}
    this.loadProduct = this.loadProduct.bind(this)
  }

  loadProduct () {
    const self = this
    $.ajax({
      url: 'http://localhost:3001/products.json',

      crossDomain: true,
      xhrFields: {
        withCredentials: true
      },
        // The name of the callback parameter, as specified by the YQL service
      jsonp: 'callback',

        // Tell jQuery we're expecting JSONP
      dataType: 'jsonp',

        // Work with the response
      success: function (response) {
        self.setState({
          products: response
        })
      }
    })
  }

  render () {
    return (
      <div>
        <Button bsStyle='primary' onClick={this.loadProduct}>Show products</Button>
        <ListGroup>
          {
              this.state.products.map(function (product, index) {
                return (<ListGroupItem key={index}>{product.name}</ListGroupItem>)
              }, this)
            }
        </ListGroup>
      </div>
    )
  }
  }

export default Product
